from bs4 import BeautifulSoup
from utils.geojson import GeoJSONBuilder, GeoJSONPoint


class WikimapiaParser():
    
    @staticmethod
    def parse_search_page_points(page_html:str):
        '''
            description: parses search output from wikimapia's website.
            converts the search output to useful json to track our environenment.
            params:
            * page_html - of the page html, used to get the data
        '''
        soup = BeautifulSoup(page_html, 'html.parser')
        search_result_items = soup.find_all('li', {"class": 'search-result-item'})
        points = []
        for result in search_result_items:
            lat = result.get('data-latitude')
            lon = result.get('data-longitude')
            strong_elem = result.find('strong')
            small_elem = result.find('small')
            if strong_elem:
                name = strong_elem.text
            elif small_elem:
                name = small_elem.text.split(',')[0]
                name = name.replace('\n', '').replace(' ', '')
            if name:
                point = GeoJSONPoint(lat, lon, {
                    "name": name
                })
                points.append(point)
        return points

    @staticmethod
    def parse_search_pages_points(page_html_list:list[str]) -> list[GeoJSONPoint]:
        '''
            description: parse_search_companies, parses the output given from each search, into a list of points in geojson
            params:
            page_html list: list of strings to check their companie's background first.
        '''
        builder = GeoJSONBuilder()
        output_points = []
        for page in page_html_list:
            points = WikimapiaParser.parse_search_page_points(page)
            output_points += points
        return output_points
    @staticmethod
    def points_to_json(points:list[GeoJSONPoint]) -> dict:
        '''
            description: changes given points in spcace(geojson), convert them to json
            params:
            * points - list of geojson points we would like to convert to file.
        '''
        builder = GeoJSONBuilder()
        for point in points:
            builder.add_geojson_point(point)
        return builder.build()
    @staticmethod
    def parse_search_pages(pages_html:list[str]) -> dict:
        '''
            parses search pages by combining all of the existing  static functions.
        '''
        builder = GeoJSONBuilder()
        for page in pages_html:
            points = WikimapiaParser.parse_search_page_points(page)
            for point in points:
                if not builder.get_point_by_property('name', point.properties['name']):
                    builder.add_geojson_point(point)
        return builder.build()