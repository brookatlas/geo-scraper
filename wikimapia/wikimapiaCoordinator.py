from utils.geo import chunk_bbox_traverser, convert_current_point
from wikimapia.wikimapiaHandler import WikimapiaHandler
from wikimapia.wikimapiaParser import WikimapiaParser
from pathos.multiprocessing import ProcessPool



class wikimapiaCoordinator():
    '''
        name: WikimapiaCoordinator
        description: searches in the wikimapia search, 
                     coordinates processes in order to scrape effectively without waiting too much time.
    '''
    def __init__(self, handler:WikimapiaHandler = WikimapiaHandler(), parser:WikimapiaParser = WikimapiaParser()):
        self.handler = handler
        self.parser = parser
        self.output = {}
    
    def search(self, q:str, bbox:str, limit:int = None) -> list:
        '''
            name: search
            description: searches in wikimapia, parses the chunk data and returns a list of points as output
            params:
                * q - query to search for
                * bbox - boundary box tuple to search in
                * limit - chunk limit to collect
        '''
        output_list = []
        c = 0
        for chunk in chunk_bbox_traverser(bbox, step_size=0.300000):
            c += 1
            output_list += self.search_and_parse_chunk(q, chunk)
            if c == limit and limit:
                break
        return output_list

    def search_and_parse_once(self, q:str, x:float, y:float) -> list:
        '''
            name: search_and_parse_once
            description: searches and parses one point of data(search in one disctintive point)
            params:
                * q - query to search for
                * x - x position to search in (lat)
                * y - y position to search in (lot)
            return value: list of parsed points
        '''
        raw_docs = self.handler.search_all(q, x, y)
        parsed_points = self.parser.parse_search_pages_points(raw_docs)
        return parsed_points
    
    def output_json(self, parsed_points) -> dict:
        '''
            name: output_json
            description: outputs a parsed list of points, into json
        '''
        return self.parser.points_to_json(parsed_points)

    def search_and_parse_chunk(self, q:str, chunk:list):
        '''
            name: search_and_parse_chunk
            description: searches and parses one chunk.
            the search is done distributedly with the use of a map.
            params: 
                * q - query to search for
                * chunk - list of points to search in
            
            ### notes
            this method is not the most effective(process per one search, 4-8 processes per average chunk).
            how could be improved?
                * could be improved by opening a process for each page of a search result, and not one per a whole search result.
        '''
        output_points = []
        arg_list = []
        pool = ProcessPool(nodes=8)
        for point in chunk:
            converted_point = convert_current_point(point[0], point[1])
            arg_list.append([q, converted_point[0], converted_point[1]])
        results = pool.map(self.search_and_parse_once, [a[0] for a in arg_list], [a[1] for a in arg_list], [a[2] for a in arg_list])
        for result in results:
            output_points += result
        return output_points