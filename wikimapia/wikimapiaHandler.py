import urllib
import random
import requests
from time import sleep
from requests import HTTPError
from pathos.pools import ProcessPool
from utils.decorators import singleton
from utils.geo import convert_current_point

requests.adapters.DEFAULT_RETRIES = 5



class WikimapiaHandler():
    '''
        description: class is in-charge of sending actual requests to wikimapia
        params:
        base_uri = usually wikimapia's uri
    '''
    def __init__(self, base_uri = "http://wikimapia.org"):
        self.base_uri = base_uri

    @property
    def search_uri(self):
        '''
            name: search_uri
            type: property

            returns wikimapia search's uri.
        '''
        return f'{self.base_uri}/search'
    
    def search_once(self, q:str, start:int = 0, x:int = None, y:int = None, z:int = None):
        '''
            name: search_once
            description: searches once in wikimapia's search endpoint.
            params:
                * q - query to search for. for example: 'mall'
                * start - start index. can be changed to traverse all the results.
                * x - latitude position
                * y - longitude position
                * z - zoom amount(22 is OK)
        '''
        try:
            uri = f'{self.search_uri}?q={q}'
            body = {
                "qu": q,
                "jtype": "simple",
                "start": start,
                "try": 0
            }
            if x:
                body["x"] = x
            if y:
                body["y"] = y
            if z:
                body["z"] = z
            body = urllib.parse.urlencode(body)
            headers = {
                'Content-type': 'application/x-www-form-urlencoded',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36 Edg/89.0.774.77'
            }
            response = requests.post(uri, data=body, headers=headers, timeout=(3, 6))
            response.raise_for_status()
            sleep_time = 1.2 + random.uniform(0.5, 1)
            sleep(sleep_time)
            errors = ["did not match any places", "maintenance"]
            for error in errors:
                if  error in response.content.decode():
                    return 'done searching', start
            return response.content.decode(), start
        except HTTPError as e:
            print('search problem')
            return None

    

    def search_all(self, q:str, x:int = None, y:int = None) -> list:
        '''
            name: search_all
            description: searches for a result in wikimapia's search.
            then traverses all of the different search results, and returns them in a list of html pages.
        '''
        print(f'searching {q} on lat:{x} lon:{y} zoom 22')
        output = []
        resp, index = '', -20
        while resp != 'done searching':
            index += 20
            print(f'searching: {q} on index: {index}')
            resp, index = self.search_once(q, index, x, y, 22)
            output.append(resp)
        return output[:len(output) - 1]
    
    
    
    
        