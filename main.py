import json
import click
from utils.geo import get_bbox_by_country_name
from utils.exceptions import CountryNotFound
from wikimapia.wikimapiaCoordinator import wikimapiaCoordinator

    
@click.group()
def cli():
    print('welcome to the geo scraper!')

@click.command()
@click.argument('country', type=str)
@click.option('--chunk-limit', type=int, default=1)
@click.option('--destination', type=str)
def scrape(country:str, chunk_limit:int, destination = None):
    '''
        description: scrape command. scrapes a country info by boundary box and chunk limits, returns a geojson file with point data only.
        options:
            * country - name of country to scrape. if not valid, an appropriate message will appear.
            * chunk-limit - optional. how many chunks to scrape? defaults to 1, for demo purposes.
            * destination - file destination to write to. defaults to 'output.json' in current directory.
    '''
    try:
        bbox = get_bbox_by_country_name(country)
        coordinator = wikimapiaCoordinator()
        points = coordinator.search(country, bbox, limit=chunk_limit)
        geo_json = coordinator.output_json(points)
        path = 'output.json'
        if destination:
            path = destination
        f = open(path, 'w+')
        f.write(json.dumps(geo_json))
        f.close()
    except CountryNotFound as e:
        print(f'country named: {country} was not found! please enter a valid country next time.')
        

cli.add_command(scrape)

if __name__ == '__main__':
    cli()