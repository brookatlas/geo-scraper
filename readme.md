## geo-scraper

basic geo-scraper written in python over wikimedia.
due to time constraints where wikimapia lately has maintenance times on evenings,
I could only make it work with points so far.
it does work distributedly over different processes, and brings quite lots of data.

### system requirements
* have python 3.x installed(3.9.x is recommended)
* have git installed properly

### installation
* `git clone https://gitlab.com/brookatlas/geo-scraper.git`
* `pip install -r requirements.txt`


### usage
basic example of usage:
* `python main.py scrape <COUNTRY_NAME>`

this example scrapes only one chunk of data(boundary box of country is divided into chunks).
#### extra params
* --chunk-limit - sets how many chunks are to be scraped. with nothing set, for demo purposes the program scapes only one chunk.
* --destination - sets the destination file json output. on default, the output is "output.json" file.