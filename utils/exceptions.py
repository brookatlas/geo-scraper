
class CountryNotFound(Exception):
    def __init__(self, country_name:str):
        self.country_name = country_name
        super().__init__(self, country_name)
    def __str__(self):
        exp_str = f'country named {country_name} was not found'
        return exp_str