

class GeoJSONPoint():
    '''
        description: represents a point in a geojson file
    '''
    def __init__(self, x:int, y:int, properties:dict):
        self.x = x
        self.y = y
        self.properties = properties

class GeoJSONBuilder():
    '''
        description: a class that builds geojson-formatted files, with minimal support only for points.
                    (for this demo)
    '''
    def __init__(self, built:dict = None):
        if built:
            self.dict = built
        else:
            self.dict = {
                'type': 'FeatureCollection',
                'features': []
            }
    def get_points(self):
        '''
            description: gets all points in a geojson file.
        '''
        points = []
        for feature in self.dict['features']:
            if feature['geometry'].get('type') == 'Point':
                points.append(feature)
        return points
    def get_point_by_property(self, property_name:str, property_value:str) -> dict:
        '''
            description: gets a point in the file, by a property with specific value
            params:
                * property_name - name of property to look for in points in the file
                * property_value - value of property to look for
            return value:
                if such matching point is found - the point is returned.(dict)
                if a no matching property is found, None is returned.
        '''
        points = self.get_points()
        for point in points:
            if point['properties'][property_name] == property_value:
                return point
        return None
    def add_point(self,x:int, y:int, properties:dict) -> None:
        '''
            description: adds a point to the geojson builder object
            params:
                * x - x coordinate for new point
                * y - y coordinate for new point
                * properties - dictionary for properties of new point
        '''
        point_dict = {
            'type': 'Feature',
            'geometry': {
                'type':'Point',
                'coordinates': [x, y]
            },
            'properties': properties
        }
        points = self.get_points()
        for point in points:
            if point['properties'].get('name') == properties.get('name'):
                return None        
        self.dict['features'].append(point_dict)
    def add_geojson_point(self, point:GeoJSONPoint) -> None:
        '''
            description: adds a point to the geojson builder object, directly straight from a GeoJSONPoint object
            params:
                * point - GeoJSONPoint object
        '''
        self.add_point(point.x, point.y, point.properties)
    def build(self) -> dict:
        '''
            description: returns the built geojson dictionary
        '''
        return self.dict