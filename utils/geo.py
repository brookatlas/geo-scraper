from utils.exceptions import CountryNotFound
from country_bounding_boxes import countries

def get_bbox_by_country_name(country_name:str) -> tuple:
    '''
        name: get_bbox_by_country_name
        description: gets a boundary box by country name
        if such country does not exist, none will be returned
    '''
    output = next(
        (country for country in countries if str.lower(country.name_long) == str.lower(country_name)),
        None
    )
    if output:
        return output.bbox
    else:
        raise CountryNotFound(country_name)



def chunk_bbox_traverser(bbox:tuple, step_size = 0.100000, chunk_size = 8):
    '''
        name: chunk_bbox_traverser
        description: a generator that traverses a boundary box points, by dividing them up into chunks.
        the chunk's size is determined by the "chunk_size" parameter.
        the number of times the country is "divided" is determined by "step_size" parameter.
        the traverser traverses the bbox across the x-axis, and when it finds its end,
        it goes back all the way to start, while stepping down in the y-axis.
        the bigger the step size, the smaller the amount of points/chunks the traverser will pass, and vice versa.

        params:
            bbox: tuple containing the 2 points representing the bbox
            step_size: size to step each time
            chunk_size: amount of steps/points of each chunk to be scaled at.
    '''
    start_lat = min(bbox[0], bbox[2])
    start_lon = min(bbox[1], bbox[3])
    end_lat = max(bbox[0], bbox[2])
    end_lot = max(bbox[1], bbox[3])
    current_lat = start_lat
    current_lot = start_lon
    while current_lot < end_lot:
        chunk_list = []
        for i in range(chunk_size):
            chunk_list.append((current_lat, current_lot))
            if current_lat < end_lat:
                current_lat += step_size
            else:
                current_lat = start_lat
                current_lot += step_size
        yield chunk_list
            

def convert_current_point(x:float, y:float) -> tuple:
    '''
        description: converts regular lat/lot float coordinates to wikimapia's float to integer format.
        wikimapia's integer format needs to be exactly 8 points after the digit(without the digit), or it won't work.
        example:
        (32.31434512312, 35.12351237856) would be converted to (32314345, 35123512), having exactly 8 digits for each number.
        taking even one digit down, would make requests to wikimapia's search to not work in relation to the point's position.

        params: 
         * x - x position(latitude)
         * y - y position(longitude)

    '''
    str_x = str(x)
    str_y = str(y)
    new_x_len = str_x.find('.') + 7
    new_y_len = str_y.find('.') + 7
    new_x = str_x[0:new_x_len + 1].replace('.', '')
    new_y = str_y[0:new_y_len + 1].replace('.', '')
    return (new_x, new_y)



